#!/bin/bash

#This software is distributed as open source according to the licenses listed below:
#https://www.gnu.org/licenses/gpl-3.0.en.html
#https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
#https://opensource.org/licenses/MIT

echo "upgrading repo to epel, adding librenms repo, and updating...."
yum install epel-release
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
yum update
echo "updating complete."

echo "installing dependencies...."
yum install composer cronie fping git httpd ImageMagick jwhois \
mariadb mariadb-server mtr MySQL-python net-snmp net-snmp-utils \
nmap php72w php72w-cli php72w-common php72w-curl php72w-gd \
php72w-mbstring php72w-mysqlnd php72w-process php72w-snmp \
php72w-xml php72w-zip python-memcached rrdtool
echo "installing dependences complete."

echo "adding librenms user to system and giving perms...."
useradd librenms -d /opt/librenms -M -r
usermod -a -G librenms apache
echo "welcome to the system user librenms, nice perms."

echo "moving to /opt and installing librenms...."
cd /opt
composer create-project --no-dev --keep-vcs librenms/librenms librenms dev-master
echo " librenms installed."

"echo running sql queries to configure database...."
systemctl start mariadb
mysql -u root -e "CREATE DATABASE librenms CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
mysql -u root -e "CREATE USER 'librenms'@'localhost' IDENTIFIED BY 'password';"
mysql -u root -e "GRANT ALL PRIVILEGES ON librenms.* TO 'librenms'@'localhost';
mysql -u root -e "FLUSH PRIVILEGES;"
echo "database configured."

echo "inserting lines into [mysqld] section of /etc/my.cf"
cat /etc/my.cnf | awk '/mysqld/{print "innodb_file_per_table=1"}1' file
cat /etc/my.cnf | awk '/innodb_file_per_table/{print "lower_case_table_names=0"}1' file
echo "inserting lines into [mysqld] section of /etc/my.cf complete."

echo "enabling and restarting database...."
systemctl enable mariadb
systemctl restart mariadb
echo "enabling and restarting database complete."

echo "setting timezone for php...."
mkdir /opt/tmp/
mkdir /opt/bak/
touch /opt/php.ini
echo "date.timezone = "US/Central" > /opt/tmp/php.ini
cat /etc/php.ini >> /opt/tmp/php.ini.txt
mv /etc/php.ini /opt/bak/
mv /opt/tmp/php.ini /etc/php.ini
echo "setting timezone for php complete."

echo "configuring apache, enabling, and restarting...."
#need to edit ServerName here
git clone https://gitlab.com/drew32/drews-nmslog/blob/master/librenms_node_1/librenms.conf /etc/httpd/conf.d/
systemctl enable httpd
systemctl restart httpd
echo "apache configured, enabled, and restarted."

echo "Installing and configuring SELinux Policy tool...."
yum install policycoreutils-python
semanage fcontext -a -t httpd_sys_content_t '/opt/librenms/logs(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/opt/librenms/logs(/.*)?'
restorecon -RFvv /opt/librenms/logs/
semanage fcontext -a -t httpd_sys_content_t '/opt/librenms/rrd(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/opt/librenms/rrd(/.*)?'
restorecon -RFvv /opt/librenms/rrd/
semanage fcontext -a -t httpd_sys_content_t '/opt/librenms/storage(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/opt/librenms/storage(/.*)?'
restorecon -RFvv /opt/librenms/storage/
semanage fcontext -a -t httpd_sys_content_t '/opt/librenms/bootstrap/cache(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/opt/librenms/bootstrap/cache(/.*)?'
restorecon -RFvv /opt/librenms/bootstrap/cache/
setsebool -P httpd_can_sendmail=1
echo "Install and configure SELinux Policy tool complete."

echo "loading fping module...."
checkmodule -M -m -o http_fping.mod http_fping.tt
semodule_package -o http_fping.pp -m http_fping.mod
semodule -i http_fping.pp
echo "fping module loaded."

echo "configure netfilter firewall...."
firewall-cmd --zone public --add-service http
firewall-cmd --permanent --zone public --add-service http
firewall-cmd --zone public --add-service https
firewall-cmd --permanent --zone public --add-service https
echo "firewall configuration complete."

echo "setting up snmpd...."
cp /opt/librenms/snmpd.conf.example /etc/snmp/snmpd.conf
#have to set your string by replacing "SETYOURSTRING" below
sed -i 's/RANDOMSTRINGGOESHERE/SETYOURSTRINGHERE/g' /etc/snmp/snmpd.conf
curl -o /usr/bin/distro https://raw.githubusercontent.com/librenms/librenms-agent/master/snmp/distro
chmod +x /usr/bin/distro
systemctl enable snmpd
systemctl restart snmpd
echo "done with snmpd."

echo "configuring cron and logrotate, then setting permissions...."
cp /opt/librenms/librenms.nonroot.cron /etc/cron.d/librenms
cp /opt/librenms/misc/librenms.logrotate /etc/logrotate.d/librenms
chown -R librenms:librenms /opt/librenms
setfacl -d -m g::rwx /opt/librenms/rrd /opt/librenms/logs /opt/librenms/bootstrap/cache/ /opt/librenms/storage/
setfacl -R -m g::rwx /opt/librenms/rrd /opt/librenms/logs /opt/librenms/bootstrap/cache/ /opt/librenms/storage/

echo "configuring cron and logrotate, then setting permissions complete."
echo "go here: http://librenms.example.com/install.php"
echo "if asked to create config.php the follow instructions then run:"
echo "chown librenms:librenms /opt/librenms/config.php"
echo "configuring cron and logrotate, then setting permissions complete....
echo "....as long as you followed instructions."

echo "Consuming all NAGIOS and WeatherMap Plugins...."
yum install nagios-plugins-all
echo "$config['show_services']           = 1;" > /opt/librenms/config.php
echo "$config['nagios_plugins']   = "/usr/lib/nagios/plugins";" > /opt/librenms/config.php
chmod +x /usr/lib/nagios/plugins/*
echo "Consuming all Nagios and WeatherMap Plgugins complete."
echo "Check here if problems: https://docs.librenms.org/Extensions/Services/"


echo "configuring LibreNMS for Graylog Integration...."
echo "$config['graylog']['server']   = 'http://GRAYLOGIP';" > /opt/librenms/config.php
echo "$config['graylog']['port']     = 9000;" > /opt/librenms/config.php
echo "$config['graylog']['username'] = 'admin';" > /opt/librenms/config.php
echo "$config['graylog']['password'] = 'admin';" > /opt/librenms/config.php
echo "$config['graylog']['version']  = '2.4'" > /opt/librenms/config.php
echo "$config['graylog']['timezone'] = 'America/Chicago';" > /opt/librenms/config.php
echo "*/5 * * * * librenms /opt/librenms/services-wrapper.py 1" > /etc/crontab
echo "LibreNMS Config for Graylog complete."
echo "can run ./check-services.php -d for debug if needed."
echo "Please see https://docs.librenms.org/Extensions/Graylog/ for more details with LibreNMS/Graylog"

echo "Next versions of script will include Grafana Integrations step here. Skipping for now."













