 Drews NMSLOG

rsyslog, graylog, and LibreNMS setup automation for feature richness
This software is distributed as open source according to the licenses listed below:
https://www.gnu.org/licenses/gpl-3.0.en.html
https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
https://opensource.org/licenses/MIT

Prereqs:
centos 7
git

Special thanks to the LibreNMS community

Special thanks to David Lang, from the rsyslog community

Special thanks to C Spire/Tek Links

Arbitrary Application Logs --> rsync/rsyslog, imfile&omudpspoof --> G
Arbitrary Application Logs -->                                      R
Arbitrary Application Logs -->                                      A
Arbitrary Application Logs -->                                      Y  Elasticsearch/mongodb --> LibreNMS --> Final Syslog Mixdown for historical alerting
Arbitrary Application Logs -->                                      L
Arbitrary Application Logs -->                                      O
Arbitrary Application Logs -->                                      G
Arbitrary Application Logs --> rsync/rsyslog, imfile&omudpspoof --> _



