#!/bin/bash

#This software is distributed as open source according to the licenses listed below:
#https://www.gnu.org/licenses/gpl-3.0.en.html
#https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
#https://opensource.org/licenses/MIT

echo "updgrading repo to epel and updating...."
yum install epel-release
yum update
echo "updating complete."

echo "installing rsyslog...."
yum install rsyslog
echo "rsyslog installed."

echo "backing up rsyslog default config and blanking...."
mv /etc/rsyslog.conf /etc/default_rsyslog.bak && cp /etc/default_rsyslog.bak /etc/default_rsyslog.bak2
touch /etc/rsyslog.conf
echo "rsyslog back up and rsyslog is now looking at blank file.


echo "getting new rsyslog.conf...."
cd /etc/
#below can obviously be subbed for wherever you want to pull your config from
git https://gitlab.com/drew32/drews-nmslog/blob/master/rsyslog_node_1/rsyslog.conf
cd /opt
echo "new rsyslog.conf complete."

echo "restarting rsyslog...."
#systemctl restart rsyslog #for systemd
service rsyslog restart #for initV
echo "restarting rsyslog complete."

echo "That's all, folks!"


