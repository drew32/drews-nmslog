
#!/bin/bash
#Most of this software is distributed under GPLv2, GPLv3, and MIT licenses linked immediately below this line:
#https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
#Others linked in readme
#exec > /home/user/setupdebug.log 2>&1 #doesn't work sometimes
#exec > >(tee -ia script.log)
BASH_XTRACEFD="5"
PS4='$LINENO: '
set -x

echo "installing epel repos, updating OS, and installing java...."
yum install epel-release
yum update
yum install java-1.8.0-openjdk-headless.x86_64
echo " done installing epel repos, updating OS, and installing java."

echo "appending data to mongodb repo...."
yum install wget
yum install yum-utils
yum-config-manager --add-repo https://gitlab.com/drew32/drews-nmslog/raw/master/graylog_node_1/mongodb-org.repo 
yum-config-manager --enable https://gitlab.com/drew32/drews-nmslog/raw/master/graylog_node_1/mongodb-org.repo
echo "done appending data to mongodb repo."

echo "installing mongodb-org...."
yum update 
yum install mongodb-org
echo "installing mongodb-org complete."

echo "config daemon, reloading, enabling, and starting mongodb...."
chkconfig --add mongod
systemctl daemon-reload
systemctl enable mongod.service
systemctl start mongod.service
echo "config daemon, reloading, enabling, and starting mongodb complete."

echo "importing elasticsearch rpm...." 
rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
yum update
echo "importing elasticsearch rpm complete."

echo "appending contents of elasticsearch repo...."
yum-config-manager --add-repo https://gitlab.com/drew32/drews-nmslog/raw/master/graylog_node_1/elasticsearch.repo
yum update 
echo "appending contents of elasticsearch repo complete."

echo "installing elasticsearch...."
yum update
sudo yum install elasticsearch-oss
echo "installing elasticsearch complete."

echo "appending data to elasticsearch.yml...."
echo "cluster.name: graylog" > /etc/elasticsearch/elasticsearch.yml
echo "action.auto_create_index: false" > /etc/elasticsearch/elasticsearch.yml
echo "appending data to elasticsearch.yml complete."

echo "adding elasticsearch daemon, reloading, enabling, and restarting the service...."
sudo chkconfig --add elasticsearch
sudo systemctl daemon-reload
sudo systemctl enable elasticsearch.service
sudo systemctl restart elasticsearch.service
echo "adding elasticsearch daemon, reloading, enabling, and restarting the service complete."

echo "adding graylog repo, installing, stand by, we might need your input here...."
sudo rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-3.0-repository_latest.rpm
sudo yum install graylog-server
#echo -n "Enter Password: " && head -1 </dev/stdin | tr -d '\n' | sha256sum | cut -d" " -f1
#echo " please see http://docs.graylog.org/en/3.0/pages/configuration/web_interface.html#configuring-webif for multi node LB, HA, and such. please note this is a single node script."
echo "http_bind_address = $hostname" /etc/graylog/server/server.conf
echo "sure hope I see this message, lol."


#Note

#If you’re operating a single-node setup and would like to use HTTPS for the Graylog web interface and the Graylog REST API, 
#it’s possible to use NGINX or Apache as a reverse proxy.

echo "installing httpd...."
sudo yum install httpd
echo "installing httpd complete."

echo "Setting variable to write to all common apache config locations. I am about to echo the variable for human to double check syntax"
APACHECONFVAR1="<VirtualHost *:80>
    ServerName graylog.example.org
    ProxyRequests Off
    <Proxy *>
        Order deny,allow
        Allow from all
    </Proxy>

    <Location />
        RequestHeader set X-Graylog-Server-URL "http://graylog.example.org/"
        ProxyPass http://127.0.0.1:9000/
        ProxyPassReverse http://127.0.0.1:9000/
    </Location>

</VirtualHost>"

cat $APACHECONFVAR1 > /etc/apache2/httpd.conf
cat $APACHECONFVAR1 > /etc/apache2/apache2.conf
cat $APACHECONFVAR1 > /etc/httpd/httpd.conf
cat $APACHECONFVAR1 > /etc/httpd/conf/httpd.conf

sudo chkconfig --add graylog-server
sudo systemctl daemon-reload
sudo systemctl enable graylog-server.service
sudo systemctl start graylog-server.service

yum install policycoreutils-python
sudo setsebool -P httpd_can_network_connect 1
echo "
sudo setsebool -P httpd_can_network_connect 1
If the policy above does not comply with your security policy, you can also allow access to each port individually:

        Graylog REST API and web interface: sudo semanage port -a -t http_port_t -p tcp 9000
        Elasticsearch (only if the HTTP API is being used): sudo semanage port -a -t http_port_t -p tcp 9200

Allow using MongoDB’s default port (27017/tcp): sudo semanage port -a -t mongod_port_t -p tcp 27017"

echo "If you run a single server environment with NGINX or Apache proxy, enabling the Graylog REST API is enough. All other rules are only required in a multi-node setup. Having SELinux disabled during installation and enabling it later, requires you to manually check the policies for MongoDB, Elasticsearch and Graylog."

echo "try it now"


#scratch area below
#https://docs.google.com/document/d/1lRj6yQL7gvCTiqP28ysFV18PYhidtXQyRueKHO4thpU/edit?usp=sharing
#https://gitlab.com/drew32/drews-nmslog/snippets/1855982/raw?inline=false
#https://gitlab.com/drew32/drews-nmslog.git



